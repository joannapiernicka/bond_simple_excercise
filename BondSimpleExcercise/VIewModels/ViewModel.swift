//
//  ViewModel.swift
//  BondSimpleExcercise
//
//  Created by P Z on 25.06.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//


import Bond
import ReactiveKit

class ViewModel: NSObject {

    var nameModel = ""
    var emailModel = Observable<String?>("")
    var passwordModel = Observable<String?>("")
    var buttonEnabled = Observable<Bool>(false)
    var sliderModel = Observable<Float?>(nil)

    
    let model = Model()
    
    override init() {
        self.nameModel = model.name
    }
    
    func getData(string: String) -> Observable<String> {
        let observableDataSource = Observable(string)
        return observableDataSource
    }

}
