//
//  ViewController.swift
//  BondSimpleExcercise
//
//  Created by P Z on 25.06.2017.
//  Copyright © 2017 Joanna Charysz-Piernicka. All rights reserved.
//

import UIKit
import Bond
import ReactiveKit

class ViewController: UIViewController {
   
    
    let viewModel = ViewModel()

    @IBOutlet weak var myTextField: UITextField!
    
    @IBOutlet weak var myLabel: UILabel!
    
    @IBOutlet weak var myButton: UIButton!
    
    @IBAction func myButton(_ sender: Any) {
        setupBindingToStringInViewModel()
    }
    
    @IBOutlet weak var labelForButton: UILabel!
    
    @IBOutlet weak var mySlider: UISlider!
    
    @IBOutlet weak var labelForSlider: UILabel!
   
    @IBOutlet weak var emailTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var loginButtonOut: UIButton!
        
    @IBAction func loginButton(_ sender: Any) {
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupBindingTextFieldToLabel()
        setupReactiveToButtonTap()
        setupBindingSlider()
        setupBindingBiderctional()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func setupBindingToStringInViewModel() {
        viewModel.getData(string: viewModel.nameModel)
            .bind(to: labelForButton)
            .dispose()
    }
    
    func setupBindingBiderctional() {
        let email = emailTextField.reactive.text
        let password = passwordTextField.reactive.text
        
        viewModel.emailModel.bidirectionalBind(to: email)
        viewModel.passwordModel.bidirectionalBind(to: password)
        viewModel.buttonEnabled.bind(to: loginButtonOut.reactive.isEnabled)
        
        combineLatest(email, password) {email, password in
            let email = email ?? ""
            let password = password ?? ""
            return email.isEmpty == false && password.isEmpty == false
        }.bind(to: loginButtonOut.reactive.isEnabled)
        
        loginButtonOut.reactive.controlEvents(.touchUpInside)
            .observeNext {event in
                print("Yet we can call func login() etc.")
                print(self.viewModel.sliderModel)
        }
    }
    
    func setupBindingTextFieldToLabel() {
        myTextField.reactive.text.observeNext {
            text in print(text ?? "")
        }
        myTextField.reactive.text
            .map {"Hi " + $0!}
            .bind(to: myLabel.reactive.text)
    }
    
    func setupReactiveToButtonTap() {
        /*myButton.reactive.controlEvents(.touchUpInside)
         .observeNext {
         event in
         print("Button was tapped ver.1")
         }*/
        
        myButton.reactive.tap
            .observeNext {
                event in
                print("Button was tapped ver.2")
        }
    }
    
    func setupBindingSlider() {
        mySlider.maximumValue = 15
        mySlider.minimumValue = 1
        mySlider.reactive.value.observeNext { (value) in
           print(value)
        }
        let slider = mySlider.reactive.value
        //viewModel.sliderModel.bidirectionalBind(to: slider)
        mySlider.reactive.value
            .map {"Value is: \($0)"}
            .bind(to: labelForSlider.reactive.text)
    }
}

